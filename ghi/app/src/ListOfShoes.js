import { Link } from "react-router-dom";

function ListOfShoes(shoeProperties) {
    return (
    <div>
        <h1>Shoe List</h1>
        <div className="card">
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Manufacturer</th>
                        <th scope="col">Model</th>
                        <th scope="col">Color</th>
                        <th scope="col">Closet and Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {shoeProperties.shoes.map((shoe) => {
                        return (
                            <tr key={shoe.id}>
                                <td><Link to={`/shoes/${shoe.id}`}>{shoe.manufacturer}</Link></td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.closet_name} - Bin: {shoe.bin_number}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        <div className="d-grid gap-2">
            <Link to="/shoes/new/" className="btn btn-outline-info">Add Shoe</Link>
        </div>
    </div>
    );
}


/* className for JSX (javascript extended) instead of class
*/
export default ListOfShoes;
