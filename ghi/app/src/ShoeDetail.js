import {useParams, useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";

function ShowShoeDetail () {
    const { id } = useParams();
    const [shoe, setShoe] = useState();


    useEffect(() => {
        async function shoeDetail() {
            const url = `http://localhost:8080/api/shoes/${id}/`;
            const response = await fetch(url);
            if (response.ok) {
                const shoe = await response.json();
                setShoe(shoe);
            } else {
                console.log("An error occurred fetching the data");
            }
        }
        shoeDetail();
    })

    const navigate = useNavigate();
    const handleDelete = async (event) => {
        event.preventDefault();

        const shoeUrl = `http://localhost:8080/api/shoes/${id}`;
        // use shoeS because that is what the url uses ^
        const fetchConfig = {
            method: "delete",
            body: JSON.stringify(shoe),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoeUrl, fetchConfig);
        if (shoeResponse.ok) {
            navigate(-1);
            navigate(0);
        };
    }

    return shoe ? (
        <div className="card m-5 p-5">
            <img src={shoe.image_url} className="card-img-top" alt="..."/>
            <div className="card-body">
                <h5 className="card-title">{shoe.manufacturer}: {shoe.model_name} - {shoe.color}</h5>
                <p className="card-text">{shoe.closet_name} - Bin: {shoe.bin_number}</p>
                <a href="#" onClick={handleDelete} className="btn btn-danger">Delete Shoe</a>
            </div>
        </div>
    ) : null
    // we dont need to map here because were not looping through each shoe,
    // we have the ID for the specific shoe that we are getting the information
    // on

    // also, for the image url/closet name to show up I had to add them to the ShoeEncoder
    // because the detail view uses the ShoeListEncoder, not the ShoeEncoder
}

export default ShowShoeDetail;
