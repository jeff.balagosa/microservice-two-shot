import React, { useState } from 'react';

function NewShoeForm (props) {
    const [manufacturer, setManufacturer] = useState("");
    const [model_name, setModelName] = useState("");
    const [color, setColor] = useState("");
    const [image_url, setImageUrl] = useState("");
    const [bin, setBin] = useState("");
// Remember JSX uses camelCase, not_snake/OrPascal!

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    };
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    };
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };
    const handleImageUrlChange = (event) => {
        const value = event.target.value;
        setImageUrl(value);
    };
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.image_url = image_url;
        data.bin = bin;


        const shoesUrl = "http://localhost:8080/api/shoes/";
        // use shoeS because that is what the url uses  ^
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoesUrl, fetchConfig);
        if (shoeResponse.ok) {
            props.getShoes();
            setManufacturer("");
            setModelName("");
            setColor("");
            setImageUrl("");
            setBin("");
        }
    };

    return (
        <div className = "card p-4 m-3 shadow p-3 mb-5 bg-body-tertiary rounded">
            <h5>Add new Shoe:</h5>
            <form onSubmit={handleSubmit} className="row g-3 p-2">
                <div className="col-md-4">
                    <label htmlFor="inputManufacturer" className="form-label">Manufacturer</label>
                    <input
                        onChange={handleManufacturerChange}
                        value={manufacturer}
                        type="text"
                        className="form-control"/>
                </div>
                <div className="col-md-4">
                    <label htmlFor="inputModelName" className="form-label">Model Name</label>
                    <input
                        onChange={handleModelNameChange}
                        value={model_name}
                        type="text"
                        className="form-control"
                    />
                </div>
                <div className="col-md-4">
                    <label htmlFor="inputColor" className="form-label">Color</label>
                    <input
                        onChange={handleColorChange}
                        value={color}
                        type="text"
                        className="form-control"
                    />
                </div>
                <label htmlFor="basic-url">Image URL</label>
                <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon3">https://example.com/</span>
                </div>
                <input
                    onChange={handleImageUrlChange}
                    value={image_url}
                    type="text"
                    className="form-control"
                    id="basic-url"
                    aria-describedby="basic-addon3"
                />
                </div>
                <div className="col-md-2">
                    <label htmlFor="inputBin" className="form-label">Select a bin</label>
                    <select
                        onChange={handleBinChange}
                        value={bin}
                        id="inputBin"
                        className="form-select"
                    >
                        {props.bins.map((bin) => (
                            <option key={bin.href} value={bin.href}>
                                {bin.bin_number}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="col-12">
                    <button type="submit" className="btn btn-outline-info">Add Shoe</button>
                </div>
            </form>
        </div>
    );
}

export default NewShoeForm;
