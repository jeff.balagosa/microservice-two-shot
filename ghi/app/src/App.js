import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import HatsList from "./HatsList";
import ShowHat from "./ShowHat";
import AddHatForm from "./AddHatForm";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListOfShoes from "./ListOfShoes";
import NewShoeForm from "./NewShoeForm";
import ShowShoeDetail from "./ShoeDetail";

function App() {
  const [hats, setHats] = useState([]);
  const [locations, setLocation] = useState([]);
  const [shoes, setShoes] = useState([]);
  const [bins, setBins] = useState([]);


  async function getHats() {
    const url = "http://localhost:8090/api/hats/";
    const response = await fetch(url);
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.log("An error occurred fetching the data");
    }
  }

  async function getLocations() {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const { locations } = await response.json();
      setLocation(locations);
    } else {
      console.log("An error occurred fetching the data");
    }
  }

  async function getShoes() {
    const url = "http://localhost:8080/api/shoes/";
    const response = await fetch(url);
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.log("An error occurred fetching the data");
    }
  }

  async function getBins() {
    const url = "http://localhost:8100/api/bins/"
    const response = await fetch(url);
    if (response.ok) {
      const { bins } = await response.json();
      setBins(bins);
    } else {
      console.log("An error occured fetching the data");
    }
  }

  useEffect(() => {
    getHats();
    getLocations();
    getShoes();
    getBins();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="list" element={<HatsList hats={hats} />} />
            <Route path=":id" element={<ShowHat />} />
            <Route path="new" element={<AddHatForm locations={locations} />} />
          </Route>
          <Route path="shoes">
            <Route path="list" element={<ListOfShoes shoes={shoes} />} />
            <Route path="new" element={<NewShoeForm getShoes={getShoes} bins={bins} />} />
            <Route path=":id" element={<ShowShoeDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
