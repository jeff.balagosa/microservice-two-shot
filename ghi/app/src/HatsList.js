import { Link } from "react-router-dom";

function HatsList(props) {
  return (
    <div className="card shadow m-5 p-3 mx-auto">
      <div className="card-body">
        <h1 className="mb-3 text-center">Hats</h1>
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Style</th>
              <th scope="col">Location</th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map((hat) => {
              return (
                <tr key={hat.id}>
                  <td>{hat.id}</td>
                  <td>
                    <Link to={`/hats/${hat.id}`}>{hat.style_name}</Link>
                  </td>
                  <td>{hat.closet_name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="d-grid gap-2 col-2 mx-auto">
          <Link to="/hats/new" className="btn btn-primary">
            Add Hat
          </Link>
        </div>
      </div>
    </div>
  );
}

export default HatsList;
