import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";

function ShowHat() {
  const { id } = useParams();
  const [hat, setHat] = useState(null);

  async function handleDelete() {
    const url = `http://localhost:8090/api/hats/${id}/`;
    const fetchOptions = {
      method: "DELETE",
    };
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      console.log("Hat deleted");
      window.location.assign("/hats/list");
    } else {
      console.log("An error occurred deleting the hat");
    }
  }

  function handleImageError(event) {
    event.target.src =
      "https://propertywiselaunceston.com.au/wp-content/themes/property-wise/images/no-image@2x.png";
  }

  useEffect(() => {
    async function fetchHat() {
      const url = `http://localhost:8090/api/hats/${id}/`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setHat(data);
      } else {
        console.log("An error occurred fetching the hat details");
      }
    }
    fetchHat();
  }, [id]);

  return (
    <div className="card shadow m-5 p-3 col-6 mx-auto">
      <div className="card-body">
        {hat ? (
          <>
            <h1 className="mb-3 text-center">
              {hat.color} {hat.style_name}
            </h1>
            <div className="d-flex justify-content-center">
              <img
                className="border border-3 border-dark"
                src={hat.picture_url}
                alt={hat.style_name}
                style={{ maxWidth: "100%", height: "auto" }}
                onError={handleImageError}
              />
            </div>
            <table className="table table-striped mt-3 shadow">
              <thead>
                <tr>
                  <th scope="col">ID #</th>
                  <th scope="col">Color</th>
                  <th scope="col">Fabric</th>
                  <th scope="col">Location</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{id}</td>
                  <td>{hat.color}</td>
                  <td>{hat.fabric}</td>
                  <td>{hat.closet_name}</td>
                </tr>
              </tbody>
            </table>
            <div onClick={handleDelete} className="d-grid gap-2 col-3 mx-auto">
              <button className="btn btn-danger">Delete Hat</button>
            </div>
          </>
        ) : (
          <div className="spinner-border m-5 mx-auto" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        )}
      </div>
    </div>
  );
}

export default ShowHat;
