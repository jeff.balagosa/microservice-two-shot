import { useState } from "react";

function AddHatForm({ locations }) {
  const [style_name, setStyleName] = useState("");
  const [color, setColor] = useState("");
  const [fabric, setFabric] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const [location, setLocation] = useState("");

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setStyleName(value);
  };

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  };

  const handleUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      style_name,
      color,
      fabric,
      picture_url,
      location,
    };

    const hatsUrl = "http://localhost:8090/api/hats/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    };

    const hatsResponse = await fetch(hatsUrl, fetchOptions);
    if (hatsResponse.ok) {
      console.log("Hat Added!");
      window.location.assign("/hats/list");
    }
  };

  return (
    <div className="card shadow m-5 p-3 col-6 mx-auto">
      <div className="card-body">
        <form onSubmit={handleSubmit} id="create-hat-form">
          <h1 className="card-title">Add Your New Hat!</h1>
          <div className="form-floating mb-3">
            <input
              onChange={handleNameChange}
              value={style_name}
              placeholder="Style"
              required
              type="text"
              name="style_name"
              id="style_name"
              className="form-control"
            />
            <label htmlFor="style_name">Style</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleColorChange}
              value={color}
              placeholder="Color"
              required
              type="text"
              name="color"
              id="color"
              className="form-control"
            />
            <label htmlFor="color">Color</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleFabricChange}
              value={fabric}
              placeholder="Fabric"
              required
              type="text"
              name="fabric"
              id="fabric"
              className="form-control"
            />
            <label htmlFor="fabric">Fabric</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleUrlChange}
              value={picture_url}
              placeholder="Picture URL (Optional)"
              required
              type="url"
              name="picture_url"
              id="picture_url"
              className="form-control"
            />
            <label htmlFor="picture_url">Picture URL (Optional)</label>
          </div>
          <p className="mb-3">Please choose which closet to add this hat to:</p>
          <select
            onChange={handleLocationChange}
            value={location}
            className="form-select mb-3"
          >
            <option value="">Choose which closet</option>
            {locations.map((location) => (
              <option key={location.href} value={location.href}>
                {location.closet_name}
              </option>
            ))}
          </select>
          <div className="d-flex justify-content-center">
            <button className="btn btn-primary col-3">Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AddHatForm;
