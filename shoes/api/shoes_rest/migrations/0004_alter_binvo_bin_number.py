# Generated by Django 4.0.3 on 2023-10-18 22:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_binvo_bin_size_binvo_closet_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='binvo',
            name='bin_number',
            field=models.SmallIntegerField(),
        ),
    ]
