from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Shoe, BinVO
import json


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "bin_number",
    ]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image_url",
        "id",
    ]

    encoder = {
        "bin": BinVODetailEncoder
    }
    
    def get_extra_data(self, o):
        return {
            "bin_number": o.bin.bin_number,
            "closet_name": o.bin.closet_name,
            }


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image_url",
        "id"
    ]

    encoder = {
        "bins": BinVODetailEncoder
    }

    def get_extra_data(self, o):
        return {
            "bin_number": o.bin.bin_number,
            "closet_name": o.bin.closet_name,
            }
# to access the closet name in shoeProperties in my list of
# shoes function I have to add closet_name here!

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    """
    GET:
    Returns a dictionary with a list of all shoes, including manufacturer,
    model, color, image, and bin number

    {
        "shoes": [
            {
                "manufacturer": manufacturer of the shoes,
                "model_name": model of the shoes,
                "color": shoe color,
                "image_url": image of the shoe,
                "bin_number": shows the number of the bin
            }
        ]
    }

    POST:
    Allows us to create a new shoe.
    {
        "manufacturer": manufacturer of the shoes,
        "model_name": model of the shoes,
        "color": shoe color,
        "image_url": image of the shoe,
        "bin_number": shows the number of the bin
    }
    """
    if request.method == "GET":
        if bin_vo_id is not None:
            shoe = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoe},
            encoder=ShoeListEncoder,
            # shoelistencoder because we are LISTING the shoes
        )
    else:  # POST
        content = json.loads(request.body)
        # when the post request is sent it hits this line of code
        # (else statement) where the json object (dictionary) is
        # parsed into a python dictionary so our code can read it
        # and the content variable can be used further in the statement
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            # bin becomes the BinVO object where the bin number
            # matches the bin href
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Bin does not exist"}, status=400)

        shoe = Shoe.objects.create(**content)
        # ** breaks content into key value pairs for shoe
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
            # allows JSON to recieve python data types (our dictionary)
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    """
    Single-object API for the Shoe model

    GET:
    Shows the detailed information on a single shoe.
    {
        "manufacturer": manufacturer of the shoes,
        "model_name": model of the shoes,
        "color": shoe color,
        "image_url": image of the shoe,
        "bin_number": shows the number of the bin
    }

    PUT:
    Updates the information that we have on the shoe
    {
    "bin_number": location where the shoe is stored
    }

    DELETE:
    Removes the shoe from our application
    """

    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "This shoe does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            # converts the body of the update(PUT) request from a javascript
            # object to a python dictionary
            shoe = Shoe.objects.get(id=pk)
            bin_vo = BinVO.objects.get(import_href=content["bin"])
            # since bin key is a string we are going to get the binVO
            # object whose import_href matches the string in the PUT
            # request
            props = [
                "manufacturer",
                "model_name",
                "color",
                "image_url",
                "bin",
                ]
            # initially my props weren't updating, but when I looked through
            # my code again I saw that the only item in my list was bin_number
            # so I tried changing bin number and it worked. Added other fields
            # to props so everything can be updated as needed.

            for prop in props:
                # props are properties of a class, this for loop is set to
                # update any prop that we list
                if prop in content:
                    if prop == "bin":
                        setattr(shoe, "bin", bin_vo)
                        continue
                        # continue says DO NOT do the rest of the code,
                        # continue to the next iteration of the loop, which
                        # ends the loop because in this case, bin is the last
                        # key in our content dictionary.
                    setattr(shoe, prop, content[prop])
                    # shoe here is the specific instance of the model Shoe
                    # prop is then set to content[prop] while looping. this
                    # has to be at the bottom because we have to parse bin
                    # from a string into a binVO object.
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
                # allows JSON to recieve python data types (our dictionary)
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "This shoe does not exist"})
            response.status_code = 404
            return response
