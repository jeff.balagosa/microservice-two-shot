from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.SmallIntegerField(null=True)
    bin_size = models.IntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    image_url = models.URLField(max_length=350)
    # you do NOT need bin_number
    bin = models.ForeignKey(
        # bin inside shoe is a foreign key to the binVO model
        BinVO,
        related_name="shoe",
        on_delete=models.CASCADE,
        null=True
    )
# bin is a foreign key to a binVO object (line 5). When the bin wasnt updating
# the issue was in the PUT request I was sending a string as the value for bin
# (line18). It did not like this because it is expecting a BinVO object
# because bin is a foreign key to the BinVO object (line5). Initially my loop
# was only looping through the props(line175-api_views) and setting my
# attributes to strings which is okay for everything other than bin because
# it wasnt a BinVO object(line5) because bin is a foreign key to the BinVO
# object(line5).


    def get_api_url(self):
        return reverse("api_list_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.manufacturer} : {self.model_name} - {self.color}"

    class Meta:
        ordering = ("manufacturer", "model_name", "color")
