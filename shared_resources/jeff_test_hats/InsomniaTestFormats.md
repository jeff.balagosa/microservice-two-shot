# Insomnia Test Format

## Hats

Use the below format for **hat** POST and PUT methods in insomnia.

```
{
  "style_name": "Fairy Blossom Crown",
  "color": "Rose Pink",
  "fabric": "Enchanted Flowers",
  "picture_url": "https://cdn.midjourney.com/2a35225f-6f6a-4864-8151-ea1427c6b2be/0_2.png"
}
```

## Locations

Use the below format for **location** POST and PUT methods in insomnia

```
{
  "closet_name": "Guest House Supply Closet",
  "section_number": 8,
  "shelf_number": 4
}
```
