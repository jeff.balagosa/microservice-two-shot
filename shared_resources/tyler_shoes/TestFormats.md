# Formatting for Tyler's Terrific Tests


# Shoes

Use this for testing POST and PUT for Shoes in Insmonia
```
{
        "manufacturer": "crocs",
        "model_name": "Classic Dreamwork Shrek Clog",
        "color": "Lime Punch",
        "image_url": "https://media.crocs.com/images/t_pdpzoom/f_auto%2Cq_auto/products/209373_3TX_ALT110/crocs",
		"bin": "/api/bins/3/"
}
```

# Bins

Use this for testing POST and PUT for Bins in Insmonia
```
{
	"closet_name": "Shrek's Closet",
	"bin_number": 4,
	"bin_size": 1
}
```
