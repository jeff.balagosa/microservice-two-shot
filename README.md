# Wardrobify

Team:

- Tyler Henderson - Shoes Microservice
- Jeff Balagosa - Hats Microservice

## Instructions for running the application

- Clone the repository.
- Run `docker volume create two-shot-pgdata` from the root directory.
- Run `docker-compose build` from the root directory.
- Run `docker-compose up` from the root directory.
- Navigate to http://localhost:3000 to view the application.

## Testing

- Files to fascilitate testing can be found in the [shared_resources/](./shared_resources/) directory.

## Design

![Design Diagram](./shared_resources/design_diagram.png)

## Shoes microservice

Before beginning work on my microservice, I cloned the repository from the specified
link on gitlab.
I then ran:
docker volume create two-shot-pgdata
docker-compose build
docker-compose up

My insomnia export will be located in the shared_resources directory.

- For my Shoe microservice I created two models. My first model was Shoe, which includes fields for the `manufacturer`, `model_name`, `color`, `image_url` and `bin`, which is a foreign key to my second model, BinVO which is a value object.
- Because the Bin model was inside of a different monolith, Wardrobe, the creation of my BinVO was necessary to allow communication between the Shoe and Wardrobe monoliths.
- BinVO includes fields `import_href`, `closet_name`, `bin_number`, and `bin_size`.
    Tyler Henderson


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

- In my approach, I created two models.
- Since the Location Model is in the `Wardrobe` microservice, I implemented a `LocationVO` within my microservice to hold the data I need to use as a foreign key within my `Hat` model.
- The LocationVO includes the `import_href` and `closet_name` attributes.
- The Hat model includes the `style_name`, `color`, `fabric`, `picture_url`, and `location` (the foreign key). I also added a `get_api_url` method to generate the API URL.
