from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Hat(models.Model):
    style_name = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    fabric = models.CharField(max_length=255)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO, related_name="hats", on_delete=models.CASCADE, null=True
    )

    def get_api_url(self):
        return reverse("api_list_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return self.style_name
