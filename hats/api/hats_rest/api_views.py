from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.http import JsonResponse


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "style_name", "color"]

    def get_extra_data(self, o):
        return {
            "location_href": o.location.import_href,
            "closet_name": o.location.closet_name,
        }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "color", "fabric", "picture_url"]

    def get_extra_data(self, o):
        return {
            "location_href": o.location.import_href,
            "closet_name": o.location.closet_name,
        }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        # if location_vo_id exists
        if location_vo_id is not None:
            # get the location object using filter
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            # else get all hats
            hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        content = json.loads(request.body)

        # get the location object and put it in the content dict
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location"}, status=400)

        # create the hat
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
